# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Post
# Register your models here.
 
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass

    from django.contrib import admin

from user.models import User
 
 
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass