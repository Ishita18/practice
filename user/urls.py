
from django.contrib import admin
from django.conf.urls import url,include
from publish import views
from order import views as a
from user import views as view
from django.views.generic import TemplateView


urlpatterns = [
	# 	url(r'^home/',view.home , name='home'),
    url(r'^admin/', admin.site.urls),
    # url(r'^(?P<slug>[a-zA-Z0-9\-]+)', views.view_post, name='view_post'),
    url(r'^verify/(?P<uuid>[a-z0-9\-]+)/',view.verify, name='verify'),

	url(r'^order/pay/$',a.order,name='order_pay'),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^payment_process/$', a.payment_process, name='payment_process' ),
    url(r'^payment_done/$', TemplateView.as_view(template_name= "paypal/payment_done.html"), name='payment_done'),
    url(r'^payment_canceled/$', TemplateView.as_view(template_name= "paypal/payment_canceled.html"), name='payment_canceled'),
 
    #stripe payment
    url(r'^payment_form/',a.payment_form, name='stripe_payment'),
    url(r'^checkout', a.checkout, name="checkout_page")



]
