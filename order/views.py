# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response ,render, get_object_or_404
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.shortcuts import render
from django.http import HttpResponse
from user import settings
from publish.models import Post
import datetime
import stripe
from django.shortcuts import redirect

stripe.api_key = settings.STRIPE_SECRET_KEY


def order(request):
 	return render_to_response('order_button.html')


def payment_process(request):
    host = request.get_host()
    paypal_dict = {
   			'business': settings.PAYPAL_RECEIVER_EMAIL ,
   			'amount': '2000',
   			'item_name': 'Test',
   			'invoice': 'Test Payment Invoice',
   			'currency_code': 'USD',
   			'notify_url': 'http://{}{}'.format(host, reverse('paypal-ipn')),
   			'return_url': 'http://{}{}'.format(host, reverse('payment_done')),
   			'cancel_return': 'http://{}{}'.format(host, reverse('payment_canceled')),
   			}		
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'paypal/payment_process.html', {'form': form })


@csrf_exempt
def payment_form(request):
    context = { "stripe_key": settings.STRIPE_PUBLIC_KEY }
    return render(request, "stripe/final_payment.html", context)


@csrf_exempt
def checkout(request):
    new_item = Post(
        author = request.user,
        created  = datetime.datetime.now()
       
    )
    if request.method == "POST":
        token    = request.POST.get("stripeToken")
    try:
        charge  = stripe.Charge.create(
            amount      = 1000,
            currency    = "usd",
            source      = request.POST.get("stripeToken"),
            description = "The product charged to the user"
        )

        new_item.charge_id   = charge.id
    except stripe.error.CardError as ce:
        return False, ce
    else:
        new_item.save()
        return HttpResponse("Thanks")


        